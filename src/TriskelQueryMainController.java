
import TriskelQuery.QueryManager;
import TriskelQuery.SnapShotReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import patternmatching.ConstantSet;
import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreQuery;
import cubetriplestore.CubeTripleStoreDeserializer;
import triple.Triple;
import triplestore.TripleStore;
import triplestore.TripleStoreDeserializer;
import triplestore.TripleStoreQuery;

public class TriskelQueryMainController {

    private static ArrayList<Integer> subjectList;
    private static ArrayList<Integer> predicateList;
    private static ArrayList<Integer> objectList;

    public static void main(String[] args) throws IOException {
        TripleStore store = new CubeTripleStore();
        TripleStoreQuery storeQuery = new CubeTripleStoreQuery((CubeTripleStore) store);
        TripleStoreDeserializer storeDeserializer = new CubeTripleStoreDeserializer((CubeTripleStore) store);
        SnapShotReader snapShotReader = new SnapShotReader(storeDeserializer);
        initSPOLists();
        snapShotReader.execute("1379090174711");
        initStore(store);
        QueryManager queryManager = new QueryManager(storeQuery);
        ConstantSet resultSet = new ConstantSet();
        boolean exit = false;
        while (!exit) {
            getQuery();
            resultSet = queryManager.execute(subjectList, predicateList, objectList);
            showResult(resultSet);
            exit = true;
        }
    }

    private static void getQuery() throws IOException {
        getSubject();
        getPredicate();
        getObject();
    }

    private static void showResult(ConstantSet resultSet) {
        if (resultSet != null){
            System.out.println("Hay coincidencia en el patrón buscado: ");
            for (int i = 0; i < resultSet.size(); i++)
                System.out.println(resultSet.get(i));
        }else{
            System.out.println("No se ha encontrado ninguna coincidencia con el patrón");
        }
    }

    protected static void getSubject() throws NumberFormatException, IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduce a Subject('?' for What) : ");
        String input = buffer.readLine();
        if (!input.equals("?"))
            subjectList.add(Integer.parseInt(input));
    }

    protected static void getPredicate() throws NumberFormatException, IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduce a Predicate('?' for What) : ");
        String input = buffer.readLine();
        if (!input.equals("?"))
            predicateList.add(Integer.parseInt(input));
    }

    protected static void getObject() throws NumberFormatException, IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduce a Object('?' for What) : ");
        String input = buffer.readLine();
        if (!input.equals("?"))
            objectList.add(Integer.parseInt(input));
    }

    private static void initStore(TripleStore store) {
        store.assertTriple(new Triple(3, 4, 6));
        store.assertTriple(new Triple(1, 4, 6));
    }



    protected static void initSPOLists() {
        subjectList = new ArrayList<>();
        predicateList = new ArrayList<>();
        objectList = new ArrayList<>();
    }
}
