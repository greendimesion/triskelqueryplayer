package TriskelQuery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import triplestore.TripleStoreDeserializer;

public class SnapShotReader {

    private TripleStoreDeserializer tripleStoreDeserializer;
    
    public SnapShotReader(TripleStoreDeserializer tripleStoreDeserializer) {
        this.tripleStoreDeserializer = tripleStoreDeserializer;
    }

    public void execute(String transaction) throws FileNotFoundException, IOException {
        String path = "TriskeldbTripleStore-" + transaction;
        File snapshot = new File(path);
        ObjectInputStream objectInputSteam = new ObjectInputStream(new FileInputStream(snapshot));
        tripleStoreDeserializer.execute(objectInputSteam);
    }
    

    
}
