package TriskelQuery;

import java.util.ArrayList;
import patternmatching.Constant;
import patternmatching.ConstantSet;
import patternmatching.Operand;
import patternmatching.Pattern;
import patternmatching.PatternQuery;
import triplestore.TripleStoreQuery;
import patternmatching.What;


public class QueryManager {
   
    private TripleStoreQuery storeQuery;

    public QueryManager(TripleStoreQuery storeQueryInterface) { 
       this.storeQuery = storeQueryInterface;
    }

    public ConstantSet execute(ArrayList<Integer> subjectList, ArrayList<Integer> predicateList, ArrayList<Integer> objectList){
        try{
        Pattern pattern = new Pattern(getOperand(subjectList), getOperand(predicateList), getOperand(objectList));
        PatternQuery patternQuery;
        patternQuery = storeQuery.createPatternQuery(pattern);
        patternQuery.start();
        patternQuery.join();
        if (patternQuery.getResult()){
            System.out.println("Hay coincidencia");
            ConstantSet resultSet = new ConstantSet();
            resultSet = patternQuery.getSet("a");
            return resultSet;
        }  
        }catch(Exception e){

        }       
        return null;
    }
    
    private Operand getOperand(ArrayList<Integer> operandList){
        switch (operandList.size()){
            case 0:
                What what = new What("a");
                return what;
            case 1:
                Constant constant = new Constant(operandList.get(0));
                return constant;
            default:
                return null;
        }
    }
}
